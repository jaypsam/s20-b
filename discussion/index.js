console.log("Hello World");

/*
	While loop - takes a single condition. If the condition is true it will run the code.

		Syntax:
			while(condition){
				statement
			}
*/

let count = 5;

while(count !== 0){
	console.log("While: " + count);
	count--;
}

let num = 0;

while(num <= 5){
	console.log("While: " + num);
	num++;
}


//activity
//Display even number increment by 2.

console.log("");
let numA = 0;

while (numA<=30){
	console.log("While: " + numA);
	numA +=2;
}

/*
	Do While Loop
		a do while loop works a lot like the while loop. But unlike while loops, do-while loops gurantee that the code will be executed at least once.

		Syntax:
			do{
				statement
			} while(expression/condition)
*/
/*
let number = Number(prompt("Give me a number: "));

do{
	console.log("Do while: " + number);
	number++;
}while(number < 10)
*/
//reverse
/*
number =Number(prompt("Give me another number: "));

do{
	console.log("Do while: " + number);
	number--;
}while(number > 10)
*/

/*
	For loop
		- more flexible that while loop and do-while loop

		- part:
			-initial value: tracks the progress of the loop.
			- condition: if true it will run the code;
			if false it will stop the iteration/ code 
			-iteration: it indicates how to advance the loop(increasing or decreasing); final expression

			Syntax:
				for (initialValue; condition; iteration){}
*/

for(let count = 0; count <=20; count++){

	console.log("For loop count:" + count);
}

let myString = "Joshua Pascua";
console.log(myString.length);

console.log(" ");
console.log(myString[0]);
console.log(myString[12]);


console.log(" ");
for(let x = 0; x < myString.length; x++){
	console.log(myString[x]);
}


console.log(" ");
let myName = "JOSEPHINE"

for(let n = 0; n < myName.length; n++){
	if(
		myName[n].toLowerCase() == "a" ||
		myName[n].toLowerCase() == "e" ||
		myName[n].toLowerCase() == "i" ||
		myName[n].toLowerCase() == "o" ||
		myName[n].toLowerCase() == "u" 
		){
		console.log("Vowel");
	}else{
		console.log(myName[n])
	}
}

/*
//Consonant
console.log(" ");
let myName2 = "extravagant"

for(let y = 0; y < myName2.length; y++){
	if(
		myName2[y].toLowerCase() == "a" ||
		myName2[y].toLowerCase() == "e" ||
		myName2[y].toLowerCase() == "i" ||
		myName2[y].toLowerCase() == "o" ||
		myName2[y].toLowerCase() == "u"  
		){
		console.log("Vowel");
	}else{
		console.log(myName2[y])
	}
}
*/

//solution

let wordOfTheDay = "extravagant";
console.log(wordOfTheDay);
let consonants = " ";
console.log(consonants)

for (n = 0; n < wordOfTheDay.length; n++) {

	if(
		wordOfTheDay[n].toLowerCase() == 'a' ||
		wordOfTheDay[n].toLowerCase() == 'e' ||
		wordOfTheDay[n].toLowerCase() == 'i' ||
		wordOfTheDay[n].toLowerCase() == 'o' ||
		wordOfTheDay[n].toLowerCase() == 'u' 
	) {
		console.log("Vowel");
		continue;
		console.log("Vowel");
	} else {
		consonants += wordOfTheDay[n]
	}
}


console.log(consonants)

// continue and Break Statement

/*
	"continue" statement allows the code to go to the next iteration without finishing the execution of all THE STATEMENT IN THE CODE BLOCK.

	"BREAK" statements on the other hand is keyword that ends the execution of the code or the current loop

*/

console.log(" ");
for (let count = 0; count <= 20; count++){

	if(count % 5 === 0){
		console.log("Div by 5");
		continue;
	}

	console.log("continue and break: " + count);
	if (count > 10){
		break;
	}
}

/*
//infinite loop
console.log(" ");
for (let count = 5; count > 1; count++){

	if(count % 5 === 0){
		console.log("Div by 5");
		continue;
	}

	console.log("continue and break: " + count);
	if (count > 10){
		break;
	}
}*/

console.log (" ");

let name = "Alexander";

for(let i = 0; i < name.length; i++);{
	console.log(name[i]);

	if(name[i].toLowerCase() === "a") {
		console.log("Continue to the iteration");
		continue;
	}

	if(name[i].toLowerCase() === "d"){
		console.log("Continue the iteration");
		break;
	}
}


















